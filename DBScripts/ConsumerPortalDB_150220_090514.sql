-- Group [Group]
create table `group` (
   `oid`  integer  not null,
   `groupname`  varchar(255),
  primary key (`oid`)
);


-- Module [Module]
create table `module` (
   `oid`  integer  not null,
   `moduleid`  varchar(255),
   `modulename`  varchar(255),
   `moduledomainname`  varchar(255),
  primary key (`oid`)
);


-- User [User]
create table `user` (
   `oid`  integer  not null,
   `username`  varchar(255),
   `password`  varchar(255),
   `email`  varchar(255),
   `first_name`  varchar(255),
   `last_name`  varchar(255),
   `birth_date`  varchar(255),
   `internal`  bit,
  primary key (`oid`)
);


-- Neutral User [ent1]
create table `neutral_user` (
   `user_oid`  integer  not null,
   `registration_date`  date,
   `family_role`  varchar(255),
   `house_holder`  bit,
   `educational_level`  varchar(255),
   `income_rate`  varchar(255),
   `currency`  varchar(255),
   `public`  bit,
   `language`  varchar(255),
   `temperature_unit`  varchar(255),
   `length_unit`  varchar(255),
  primary key (`user_oid`)
);


-- District [ent10]
create table `district` (
   `oid`  integer  not null,
   `zipcode`  varchar(255),
   `country`  varchar(255),
   `city`  varchar(255),
   `name`  varchar(255),
  primary key (`oid`)
);


-- Weather Condition [ent11]
create table `weather_condition` (
   `oid`  integer  not null,
   `start_date`  date,
   `end_date`  date,
   `rain_fall`  decimal(19,2),
   `average_temperature`  decimal(19,2),
  primary key (`oid`)
);


-- Consumer Segment [ent12]
create table `consumer_segment` (
   `oid`  integer  not null,
   `name`  varchar(255),
   `description`  varchar(255),
  primary key (`oid`)
);


-- Feature [ent13]
create table `feature` (
   `oid`  integer  not null,
   `type`  varchar(255),
   `level`  integer,
  primary key (`oid`)
);


-- Meter Reading [ent14]
create table `meter_reading` (
   `oid`  integer  not null,
   `reading_date_time`  datetime,
   `meter_id`  varchar(255),
   `company`  varchar(255),
   `total_consumption`  decimal(19,2),
  primary key (`oid`)
);


-- Device Consumption [ent15]
create table `device_consumption` (
   `oid`  integer  not null,
   `start_date`  datetime,
   `end_date`  datetime,
   `device_consumption`  decimal(19,2),
  primary key (`oid`)
);


-- Device Class [ent16]
create table `device_class` (
   `oid`  integer  not null,
   `name`  varchar(255),
   `number`  integer,
  primary key (`oid`)
);


-- Building [ent17]
create table `building` (
   `oid`  integer  not null,
   `building_garden_area`  decimal(19,2),
   `building_pool_volume`  decimal(19,2),
   `age`  integer,
   `building_size`  decimal(19,2),
   `residence_type`  varchar(255),
   `address`  varchar(255),
  primary key (`oid`)
);


-- Household Consumption [ent18]
create table `household_consumption` (
   `oid`  integer  not null,
   `consumption`  decimal(19,2),
   `start_date`  datetime,
   `end_date`  datetime,
  primary key (`oid`)
);


-- Tip [ent2]
create table `tip` (
   `oid`  integer  not null,
   `name`  varchar(255),
   `header`  varchar(255),
   `body`  longtext,
   `tipdate`  date,
  primary key (`oid`)
);


-- Alert [ent3]
create table `alert` (
   `oid`  integer  not null,
   `type`  varchar(255),
   `level`  integer,
   `date`  datetime,
  primary key (`oid`)
);


-- Mail [ent4]
create table `mail` (
   `oid`  integer  not null,
   `description`  varchar(255),
   `subject`  varchar(255),
   `body`  longtext,
   `language`  varchar(255),
  primary key (`oid`)
);


-- Household [ent5]
create table `household` (
   `oid`  integer  not null,
   `utilityid`  varchar(255),
   `household_size`  decimal(19,2),
   `ownership`  bit,
   `number_occupants`  integer,
   `number_pets`  integer,
   `household_garden_area`  decimal(19,2),
   `household_pool_volume`  decimal(19,2),
   `second`  bit,
   `public`  bit,
   `visible`  bit,
  primary key (`oid`)
);


-- Bill [ent6]
create table `bill` (
   `oid`  integer  not null,
   `account_number`  varchar(255),
   `bill_date`  date,
   `company`  varchar(255),
   `volume_charge`  decimal(19,2),
   `service_charge`  decimal(19,2),
   `currency`  varchar(255),
   `volume_eur_charge`  decimal(19,2),
   `service_eur_charge`  decimal(19,2),
   `exchange_rate`  decimal(19,2),
   `exchange_date`  date,
  primary key (`oid`)
);


-- MediaAsset [ent7]
create table `mediaasset` (
   `oid`  integer  not null,
   `title`  varchar(255),
   `description`  varchar(255),
   `duration`  decimal(19,2),
   `author`  varchar(255),
   `media`  varchar(255),
  primary key (`oid`)
);


-- Unit Of Measurement [ent8]
create table `unit_of_measurement` (
   `oid`  integer  not null,
   `physical_quantity`  varchar(255),
   `primary_unit`  varchar(255),
   `secondary_unit`  varchar(255),
   `conversion_coefficient`  decimal(19,2),
  primary key (`oid`)
);


-- Billing Price [ent9]
create table `billing_price` (
   `oid`  integer  not null,
   `month`  varchar(255),
   `year`  integer,
   `company`  varchar(255),
   `monthly_service_charge`  decimal(19,2),
   `monthly_volume_charge`  decimal(19,2),
  primary key (`oid`)
);


-- Group_DefaultModule [Group2DefaultModule_DefaultModule2Group]
alter table `group`  add column  `module_oid`  integer;
alter table `group`   add index fk_group_module (`module_oid`), add constraint fk_group_module foreign key (`module_oid`) references `module` (`oid`);


-- Group_Module [Group2Module_Module2Group]
create table `group_module` (
   `group_oid`  integer not null,
   `module_oid`  integer not null,
  primary key (`group_oid`, `module_oid`)
);
alter table `group_module`   add index fk_group_module_group (`group_oid`), add constraint fk_group_module_group foreign key (`group_oid`) references `group` (`oid`);
alter table `group_module`   add index fk_group_module_module (`module_oid`), add constraint fk_group_module_module foreign key (`module_oid`) references `module` (`oid`);


-- User_DefaultGroup [User2DefaultGroup_DefaultGroup2User]
alter table `user`  add column  `group_oid`  integer;
alter table `user`   add index fk_user_group (`group_oid`), add constraint fk_user_group foreign key (`group_oid`) references `group` (`oid`);


-- User_Group [User2Group_Group2User]
create table `user_group` (
   `user_oid`  integer not null,
   `group_oid`  integer not null,
  primary key (`user_oid`, `group_oid`)
);
alter table `user_group`   add index fk_user_group_user (`user_oid`), add constraint fk_user_group_user foreign key (`user_oid`) references `user` (`oid`);
alter table `user_group`   add index fk_user_group_group (`group_oid`), add constraint fk_user_group_group foreign key (`group_oid`) references `group` (`oid`);


-- Neutral User_Tip [rel1]
create table `neutral_user_tip` (
   `neutral_user_oid`  integer not null,
   `tip_oid`  integer not null,
  primary key (`neutral_user_oid`, `tip_oid`)
);
alter table `neutral_user_tip`   add index fk_neutral_user_tip_neutral_us (`neutral_user_oid`), add constraint fk_neutral_user_tip_neutral_us foreign key (`neutral_user_oid`) references `neutral_user` (`user_oid`);
alter table `neutral_user_tip`   add index fk_neutral_user_tip_tip (`tip_oid`), add constraint fk_neutral_user_tip_tip foreign key (`tip_oid`) references `tip` (`oid`);


-- Consumer Segment_Neutral User [rel10]
create table `consumer_segment_neutral_user` (
   `consumer_segment_oid`  integer not null,
   `neutral_user_oid`  integer not null,
  primary key (`consumer_segment_oid`, `neutral_user_oid`)
);
alter table `consumer_segment_neutral_user`   add index fk_consumer_segment_neutral_us (`consumer_segment_oid`), add constraint fk_consumer_segment_neutral_us foreign key (`consumer_segment_oid`) references `consumer_segment` (`oid`);
alter table `consumer_segment_neutral_user`   add index fk_consumer_segment_neutral_2 (`neutral_user_oid`), add constraint fk_consumer_segment_neutral_2 foreign key (`neutral_user_oid`) references `neutral_user` (`user_oid`);


-- Feature_Consumer Segment [rel11]
alter table `feature`  add column  `consumer_segment_oid`  integer;
alter table `feature`   add index fk_feature_consumer_segment (`consumer_segment_oid`), add constraint fk_feature_consumer_segment foreign key (`consumer_segment_oid`) references `consumer_segment` (`oid`);


-- Meter Reading_House [rel12]
alter table `meter_reading`  add column  `building_oid`  integer;
alter table `meter_reading`   add index fk_meter_reading_building (`building_oid`), add constraint fk_meter_reading_building foreign key (`building_oid`) references `building` (`oid`);


-- Device Consumption_Device [rel14]
alter table `device_consumption`  add column  `device_class_oid`  integer;
alter table `device_consumption`   add index fk_device_consumption_device_c (`device_class_oid`), add constraint fk_device_consumption_device_c foreign key (`device_class_oid`) references `device_class` (`oid`);


-- Device_House [rel15]
alter table `device_class`  add column  `household_oid`  integer;
alter table `device_class`   add index fk_device_class_household (`household_oid`), add constraint fk_device_class_household foreign key (`household_oid`) references `household` (`oid`);


-- Building_Household [rel17]
alter table `household`  add column  `building_oid`  integer;
alter table `household`   add index fk_household_building (`building_oid`), add constraint fk_household_building foreign key (`building_oid`) references `building` (`oid`);


-- Household Consumption_Household [rel18]
alter table `household_consumption`  add column  `household_oid`  integer;
alter table `household_consumption`   add index fk_household_consumption_house (`household_oid`), add constraint fk_household_consumption_house foreign key (`household_oid`) references `household` (`oid`);


-- Alert_Neutral User [rel2]
alter table `alert`  add column  `neutral_user_oid`  integer;
alter table `alert`   add index fk_alert_neutral_user (`neutral_user_oid`), add constraint fk_alert_neutral_user foreign key (`neutral_user_oid`) references `neutral_user` (`user_oid`);


-- Mail_Alert [rel3]
alter table `alert`  add column  `mail_oid`  integer;
alter table `alert`   add index fk_alert_mail (`mail_oid`), add constraint fk_alert_mail foreign key (`mail_oid`) references `mail` (`oid`);


-- House_Neutral User [rel4]
alter table `neutral_user`  add column  `household_oid`  integer;
alter table `neutral_user`   add index fk_neutral_user_household (`household_oid`), add constraint fk_neutral_user_household foreign key (`household_oid`) references `household` (`oid`);


-- House_Bill [rel5]
alter table `bill`  add column  `household_oid`  integer;
alter table `bill`   add index fk_bill_household (`household_oid`), add constraint fk_bill_household foreign key (`household_oid`) references `household` (`oid`);


-- Neutral User_MediaAsset [rel6]
create table `neutral_user_mediaasset` (
   `neutral_user_oid`  integer not null,
   `mediaasset_oid`  integer not null,
  primary key (`neutral_user_oid`, `mediaasset_oid`)
);
alter table `neutral_user_mediaasset`   add index fk_neutral_user_mediaasset_neu (`neutral_user_oid`), add constraint fk_neutral_user_mediaasset_neu foreign key (`neutral_user_oid`) references `neutral_user` (`user_oid`);
alter table `neutral_user_mediaasset`   add index fk_neutral_user_mediaasset_med (`mediaasset_oid`), add constraint fk_neutral_user_mediaasset_med foreign key (`mediaasset_oid`) references `mediaasset` (`oid`);


-- Billing Price_Bill [rel7]
create table `billing_price_bill` (
   `billing_price_oid`  integer not null,
   `bill_oid`  integer not null,
  primary key (`billing_price_oid`, `bill_oid`)
);
alter table `billing_price_bill`   add index fk_billing_price_bill_billing (`billing_price_oid`), add constraint fk_billing_price_bill_billing foreign key (`billing_price_oid`) references `billing_price` (`oid`);
alter table `billing_price_bill`   add index fk_billing_price_bill_bill (`bill_oid`), add constraint fk_billing_price_bill_bill foreign key (`bill_oid`) references `bill` (`oid`);


-- District_House [rel8]
alter table `building`  add column  `district_oid`  integer;
alter table `building`   add index fk_building_district (`district_oid`), add constraint fk_building_district foreign key (`district_oid`) references `district` (`oid`);


-- District_Weather Condition [rel9]
alter table `weather_condition`  add column  `district_oid`  integer;
alter table `weather_condition`   add index fk_weather_condition_district (`district_oid`), add constraint fk_weather_condition_district foreign key (`district_oid`) references `district` (`oid`);


-- GEN FK: Neutral User --> User
alter table `neutral_user`   add index fk_neutral_user_user (`user_oid`), add constraint fk_neutral_user_user foreign key (`user_oid`) references `user` (`oid`);


