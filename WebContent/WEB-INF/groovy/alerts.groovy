#input user
#output json

import groovy.sql.Sql;
import java.sql.Connection;
import com.webratio.rtx.db.DBTransaction;
import com.webratio.rtx.db.HibernateService;
import com.webratio.rtx.core.BeanHelper;
import com.fasterxml.jackson.core.JsonEncoding
import com.fasterxml.jackson.core.JsonFactory
import com.fasterxml.jackson.core.JsonGenerationException
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.JsonMappingException
import java.io.OutputStream
import java.io.ByteArrayOutputStream

	
Connection conn = null
def databaseId = "db1"
def session = getDBSession(databaseId)
def query1 = null
def query2 = null
def query3 = null


query1 = "select level from alert where type = 'Water quality alert' and neutral_user_oid='"+user[0]+"' order by date desc limit 1"
query2 = "select level from alert where type = 'Leakage alert' and neutral_user_oid='"+user[0]+"' order by date desc limit 1"
query3 = "select level from alert where type = 'Shortage alert' and neutral_user_oid='"+user[0]+"' order by date desc limit 1"

def result1=null
def result2=null
def result3=null
def jsonString = null

def ris=0
result1 = session.createSQLQuery(query1).list()
result2 = session.createSQLQuery(query2).list()
result3 = session.createSQLQuery(query3).list()



try {

    OutputStream out = new ByteArrayOutputStream()
    
    JsonFactory jfactory = new JsonFactory();

    /*** write to file ***/
    JsonGenerator jGenerator = jfactory.createJsonGenerator(out, JsonEncoding.UTF8);

    jGenerator.writeStartArray()
	jGenerator.writeStartObject()

	jGenerator.writeNumberField("quality", result1[0]) 
	jGenerator.writeNumberField("leakage", result2[0]) 
	jGenerator.writeNumberField("shortage", result3[0])

	jGenerator.writeEndObject()
	jGenerator.writeEndArray()

    jGenerator.close()

    json = out.toString()

} catch (JsonGenerationException e) {

    e.printStackTrace();

} catch (JsonMappingException e) {

    e.printStackTrace();

} catch (IOException e) {

    e.printStackTrace();

}

commit(session)
println json
return ["json" : json]