#input Double[] readings, Timestamp[] timestamps
#output String json

import com.fasterxml.jackson.core.JsonEncoding
import com.fasterxml.jackson.core.JsonFactory
import com.fasterxml.jackson.core.JsonGenerationException
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.JsonMappingException
import java.io.OutputStream
import java.io.ByteArrayOutputStream
import java.sql.Timestamp
import java.util.Date

def json = null

try {

	def avg = 0;
	def sum;
	def count = 0;
	Date date= new Date();
	Timestamp now = new Timestamp(date.getTime());
	Date dateBefore = new Date(now.getTime() - 7 * 24 * 3600 * 1000 );
	
	for (int i = 0; i < readings.size(); i++) {

	Date temp = new Date(timestamps[i].getTime())

		if(temp.after(dateBefore)){

			sum = sum + readings[i];
			count = count + 1; 
		}
	}
    OutputStream out = new ByteArrayOutputStream()

    JsonFactory jfactory = new JsonFactory();

    /*** write to file ***/
    JsonGenerator jGenerator = jfactory.createJsonGenerator(out, JsonEncoding.UTF8);

    jGenerator.writeStartArray(); 
	if(count>0){
		avg = sum/count;

	}
	jGenerator.writeStartObject(); 
	jGenerator.writeNumberField("avg", avg);   
	jGenerator.writeEndObject(); 
	jGenerator.writeEndArray();

    jGenerator.close();
 
    json = out.toString()

} catch (JsonGenerationException e) {

    e.printStackTrace();

} catch (JsonMappingException e) {

    e.printStackTrace();

} catch (IOException e) {

    e.printStackTrace();

}

return ["json": json]