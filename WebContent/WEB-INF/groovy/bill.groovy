#input house
#output json

import groovy.sql.Sql;
import java.sql.Connection;
import com.webratio.rtx.db.DBTransaction;
import com.webratio.rtx.db.HibernateService;
import com.webratio.rtx.core.BeanHelper;
import com.fasterxml.jackson.core.JsonEncoding
import com.fasterxml.jackson.core.JsonFactory
import com.fasterxml.jackson.core.JsonGenerationException
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.JsonMappingException
import java.io.OutputStream
import java.io.ByteArrayOutputStream

	
Connection conn = null
def databaseId = "db1"
def session = getDBSession(databaseId)
def query1 = null
def query2 = null
def query3 = null


query1 = "select service_charge, volume_charge, bill_date from bill where household_oid='"+house[0]+"' order by bill_date desc limit 1"

def result1=null
def jsonString = null

def ris=0
result1 = session.createSQLQuery(query1).list()[0]


try {

    OutputStream out = new ByteArrayOutputStream()
    
    JsonFactory jfactory = new JsonFactory();

    /*** write to file ***/
    JsonGenerator jGenerator = jfactory.createJsonGenerator(out, JsonEncoding.UTF8);

    jGenerator.writeStartArray()
	jGenerator.writeStartObject()
	jGenerator.writeNumberField("bill", result1[0] + result1[1]) 
	jGenerator.writeStringField("bill_date", result1[2].toString())
	jGenerator.writeEndObject()
	jGenerator.writeEndArray()

    jGenerator.close()

    json = out.toString()

} catch (JsonGenerationException e) {

    e.printStackTrace();

} catch (JsonMappingException e) {

    e.printStackTrace();

} catch (IOException e) {

    e.printStackTrace();

}

commit(session)

return ["json" : json]