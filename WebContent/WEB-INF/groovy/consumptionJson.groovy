#input Timestamp[] timestamps, Double[] quantities
#output String json

import com.fasterxml.jackson.core.JsonEncoding
import com.fasterxml.jackson.core.JsonFactory
import com.fasterxml.jackson.core.JsonGenerationException
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.JsonMappingException
import java.io.OutputStream
import java.io.ByteArrayOutputStream

def json = null

try {


    OutputStream out = new ByteArrayOutputStream()
    
    JsonFactory jfactory = new JsonFactory();

    /*** write to file ***/
    JsonGenerator jGenerator = jfactory.createJsonGenerator(out, JsonEncoding.UTF8);

    jGenerator.writeStartArray(); 
	timestamps.eachWithIndex{obj, idx ->
	    jGenerator.writeStartObject(); 
	    jGenerator.writeStringField("timestamp", timestamps[idx].toString());  
	    jGenerator.writeNumberField("quantity", quantities[idx]);  
	    jGenerator.writeEndObject(); 
	}
	 jGenerator.writeEndArray();

    jGenerator.close();
 
    json = out.toString()

} catch (JsonGenerationException e) {

    e.printStackTrace();

} catch (JsonMappingException e) {

    e.printStackTrace();

} catch (IOException e) {

    e.printStackTrace();

}

return ["json": json]