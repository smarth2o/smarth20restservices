#input user_oid

import groovy.sql.Sql;
import java.sql.Connection;
import com.webratio.rtx.db.DBTransaction;
import com.webratio.rtx.db.HibernateService;
import com.webratio.rtx.core.BeanHelper;

def nicknames	
Connection conn = null
def dbId = "db1"

def databaseId = "db1"
def session = getDBSession(databaseId)

def are=BeanHelper.asStringArray(area)
def upper=null
def query=null
def empty=0

query="select u.oid,r.latitude,r.longitude, u.first_name, u.last_name from community_user r, user_information u where r.oid=u.oid  "

def wh=0
/*if((BeanHelper.isNullOrEmptyString(name))&&(BeanHelper.isNullOrEmptyString(area))&&(BeanHelper.isNullOrEmptyString(geo_area))&&(BeanHelper.asString(levelmin)=="1")&&(BeanHelper.asString(levelmax)=="9")&&(BeanHelper.isNullOrEmptyString(levelmin))&&(BeanHelper.isNullOrEmptyString(levelmax))){
	query=query
}*/
if(!BeanHelper.isNullOrEmptyString(name)){
upper=name.toUpperCase()
empty=1
	query=query+" and (upper(u.first_name) in ('"+upper+"') or upper(u.last_name) in ('"+upper+"') or upper(u.company_name) like upper('%"+name+"%'))" 
	wh=1
}
if(!BeanHelper.isNullOrEmptyString(geo_area)){
	empty=1
	query=query+" and u.area_geografica='"+geo_area+"'" 
	wh=1
}
if((!BeanHelper.isNullOrEmptyArray(are))&&(!BeanHelper.isNullOrEmptyString(area))){
	k=0
	empty=1
	query=query+" and u.oid in "
	while(are.size()>k){
		if(k==0){
			query=query+"(select a.oid from mostimportant_badge m,community_user a where m.rankoid=a.oid and importance>="+levelmin+" and importance<="+levelmax+" and area in ('"+are[k]+"')) "
		
		}else{
			query=query+" and u.oid in (select a.oid from mostimportant_badge m,community_user a where m.rankoid=a.oid and importance>="+levelmin+" and importance<="+levelmax+" and area in ('"+are[k]+"')) "
			}
		k++
	}
	 

}

if((BeanHelper.isNullOrEmptyString(area))&&(!BeanHelper.isNullOrEmptyString(levelmin))&&(!BeanHelper.isNullOrEmptyString(levelmax))&&((BeanHelper.asString(levelmin)!="0")||(BeanHelper.asString(levelmax)!="9"))){
	empty=1
	query=query+" and u.oid in "
	query=query+" (select a.oid from mostimportant_badge m,community_user a where m.rankoid=a.oid and importance>="+levelmin+" and importance<="+levelmax+"  )"
	
}
if((!BeanHelper.isNullOrEmptyString(distancemin))&&(!BeanHelper.isNullOrEmptyString(distancemax))){
	empty=1
	query=query+"and (r.oid="+my_id[0]+" or (acos(sin("+latitude[0]+") * sin(r.latitude) + cos("+latitude[0]+") * cos(r.latitude) * cos(r.longitude - "+longitude[0]+")) * 6371 <= "+distancemax+") AND (acos(sin("+latitude[0]+") * sin(r.latitude) + cos("+latitude[0]+") * cos(r.latitude) * cos(r.longitude - "+longitude[0]+")) * 6371)>="+distancemin+")" 
	wh=1
}
 println query
def list=[]
def m = []
def t = []
def i = []

def mylat_degree = latitude[0]/3.14159265359*180
def mylon_degree = longitude[0]/3.14159265359*180
def myself_degree = mylat_degree+","+mylon_degree
m.add(myself_degree)
t.add("myself")
i.add("myself")


def ris=0
def result=null
if(empty==0){
//No filters
ris=1
}else{
result = session.createSQLQuery(query).list()
for (r in result){
	list.add(r[0])
	ris++
	//convert from radians to degree
	def lat_degree = r[1]/3.14159265359*180
	def lon_degree = r[2]/3.14159265359*180
	def coord = lat_degree+","+lon_degree
	m.add(coord)

	if(r[0]==my_id[0]){
		t.add("myself")
		i.add("myself")
	}
	else{
		t.add(r[3])
		i.add(r[4])
	}

	
}
}
commit(session)
if(ris==0){
list.add("HQ.NoResults.HQ")
}
return ["nicknames" : list,"markers" : m, "titles" : t, "infos" : i, "myself":myself_degree, "number_ris":ris]