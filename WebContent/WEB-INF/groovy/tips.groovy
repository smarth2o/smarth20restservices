#input String[] names, String[] headers, String[] bodies, Date[] dates
#output String json

import com.fasterxml.jackson.core.JsonEncoding
import com.fasterxml.jackson.core.JsonFactory
import com.fasterxml.jackson.core.JsonGenerationException
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.JsonMappingException
import java.io.OutputStream
import java.io.ByteArrayOutputStream

def json = null

try {

    OutputStream out = new ByteArrayOutputStream()
    
    JsonFactory jfactory = new JsonFactory();

    /*** write to file ***/
    JsonGenerator jGenerator = jfactory.createJsonGenerator(out, JsonEncoding.UTF8);

    jGenerator.writeStartArray(); 
	names.eachWithIndex{obj, idx ->
	    jGenerator.writeStartObject(); 
	    jGenerator.writeStringField("name", names[idx]);  
	    jGenerator.writeStringField("header", headers[idx]);  
	    jGenerator.writeStringField("body", bodies[idx]);
	    jGenerator.writeStringField("date", dates[idx].toString());  
	    jGenerator.writeEndObject(); 
	}
	 jGenerator.writeEndArray();

    jGenerator.close();
 
    json = out.toString()

} catch (JsonGenerationException e) {

    e.printStackTrace();

} catch (JsonMappingException e) {

    e.printStackTrace();

} catch (IOException e) {

    e.printStackTrace();

}

return ["json": json]