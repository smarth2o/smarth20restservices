#input String[] titles, String[] descriptions, String[] authors, Double[] durations, String[] medias
#output String json

import com.fasterxml.jackson.core.JsonEncoding
import com.fasterxml.jackson.core.JsonFactory
import com.fasterxml.jackson.core.JsonGenerationException
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.JsonMappingException
import java.io.OutputStream
import java.io.ByteArrayOutputStream

def json = null

try {

    OutputStream out = new ByteArrayOutputStream()
    
    JsonFactory jfactory = new JsonFactory();

    /*** write to file ***/
    JsonGenerator jGenerator = jfactory.createJsonGenerator(out, JsonEncoding.UTF8);

    jGenerator.writeStartArray(); 
	titles.eachWithIndex{obj, idx ->
	    jGenerator.writeStartObject(); 
	    jGenerator.writeStringField("title", titles[idx]);  
	    jGenerator.writeStringField("description", descriptions[idx]);  
	    jGenerator.writeStringField("author", authors[idx]); 
	    jGenerator.writeNumberField("duration", durations[idx]); 
	    jGenerator.writeStringField("media", medias[idx]);
	    jGenerator.writeEndObject(); 
	}
	 jGenerator.writeEndArray();

    jGenerator.close();
 
    json = out.toString()

} catch (JsonGenerationException e) {

    e.printStackTrace();

} catch (JsonMappingException e) {

    e.printStackTrace();

} catch (IOException e) {

    e.printStackTrace();

}

return ["json": json]